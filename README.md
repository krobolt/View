[![SensioLabsInsight](https://insight.sensiolabs.com/projects/a7683470-5532-4b30-8707-f6e8d9e37556/small.png)](https://insight.sensiolabs.com/projects/a7683470-5532-4b30-8707-f6e8d9e37556)
[![Build Status](https://travis-ci.org/djsmithme/View.svg?branch=master)](https://travis-ci.org/djsmithme/View)
[![Test Coverage](https://codeclimate.com/github/djsmithme/View/badges/coverage.svg)](https://codeclimate.com/github/djsmithme/View/coverage)

# View

Viewer for rendering templates.

#Useage.

```
$twigLoader = new \Twig_Loader_Filesystem('/Path/to/Templates');
$twigEnvironment = new \Twig_Environment(
    $twigLoader,
    ['cache' => '/Path/to/CacheDir']
);

$twigEnvironment->addExtension(new \Twig_Extension_Optimizer());
$twigLoader->addPath('/Bookings/View/Themes/Default','Default');


$smarty = new \Cyberhut\View\TemplateEngine\SmartyEngine(
    '/Path/to/Templates',
    '/smarty/compile',
    '/Path/to/CacheDir',
    /smarty/config'
);

```
Create Viewer with CacheInterface
```
$view = new \Ds\View\Viewer(
    new \Ds\View\TemplateEngine\TwigEngine($twigLoader,$twigEnvironment),
    new \Ds\View\Cache()
);
```
Call from controller
```
echo (string)$view->render('home/index.twig',[], ['cached' => 'true']);
```
Make changes to the original Twig Classes (TwigLoader/TwigEnvironment)
```
$templateEngine = $view->getTemplateEngine();
$twigEnvironment = $templateEngine->getEnvironment();
$twigLoader = $templateEngine->getLoader();
```
Change Engines
```
$view->withTemplateEngine($smarty)
echo $view->render('index.tpl',['data' => 'foo']);
```
Cache render response (memcached)
```
$cache = new \Ds\View\Cache(
    new \Ds\View\MemcacheStorage(
        new \Memcached()
    )
);

$cachedView = $view->withCache($cache);
echo $cachedView->render('home/index.twig',[], ['cached' => 'true', 'expire' => 3600);
echo $cachedView->render('home/index.twig',[], ['cached' => 'true', 'expire' => 3600); //from memcache

