<?php
namespace Tests\Ds\View\TemplateEngine;

use Ds\View\TemplateEngine\Twig1xEngine;

/**
 * Class Twig1xTest
 * @package Tests\Ds\View\TemplateEngine
 */
class Twig1xTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var
     */
    public $twigLoader;
    /**
     * @var
     */
    public $twigEnv;

    /**
     * @var Twig1xEngine
     */
    public $twigEngine;

    /**
     *
     */
    public function setUp()
    {
        $this->twigLoader = $this->getMockBuilder('\Twig_Loader_Filesystem')->getMock();
        $this->twigEnv = $this->getMockBuilder('\Twig_Environment')->getMock();
        $this->twigEngine = new Twig1xEngine($this->twigLoader, $this->twigEnv);
    }

    /**
     *
     */
    public function testRenderCache()
    {
        $this->twigEnv->expects($this->once())
            ->method('render');

        $this->twigEngine->render('path', [], ['cache' => true]);
    }

    /**
     *
     */
    public function testAddPath()
    {
        $this->twigLoader->expects($this->once())
            ->method('addPath');
        $this->twigEngine->addPath('path', 'namespace');
    }

    /**
     *
     */
    public function testGetLoader()
    {
        $loader = $this->twigEngine->getLoader();
        $this->assertSame($this->twigLoader, $loader);
    }

    /**
     *
     */
    public function testGetEnvironment()
    {
        $loader = $this->twigEngine->getEnvironment();
        $this->assertSame($this->twigEnv, $loader);
    }

    /**
     *
     */
    public function testWithLoader()
    {
        $twigLoader = $this->getMockBuilder('\Twig_Loader_Filesystem')->getMock();
        $view = $this->twigEngine->withLoader($twigLoader);
        $this->assertNotSame($this->twigLoader, $view->getLoader());
    }

    /**
     *
     */
    public function testWithEnvironment()
    {
        $twigEnv = $this->getMockBuilder('\Twig_Environment')->getMock();
        $view = $this->twigEngine->withEnvironment($twigEnv);
        $this->assertNotSame($this->twigEnv, $view->getEnvironment());
    }
}
