<?php

namespace Tests\Ds\View;

use Ds\View\CacheInterface;
use Ds\View\EngineInterface;
use Ds\View\Viewer;

class ViewerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var EngineInterface
     */
    public $engine;

    /**
     * @var CacheInterface
     */
    public $cache;
    /**
     *
     * @var Viewer
     */
    public $view;

    public function setUp()
    {
        $this->engine = $this->getMockBuilder('\Ds\View\EngineInterface')->getMock();
        $this->cache = $this->getMockBuilder('\Ds\View\CacheInterface')->getMock();
        $this->view = new Viewer($this->engine, $this->cache);
    }

    public function testWithCache()
    {
        $newCacheObject = clone $this->cache;
        $newView = $this->view->withCache($newCacheObject);
        $this->assertNotSame($this->view->getCache(), $newView->getCache());
    }

    public function testGetTemplateId()
    {
        $path = '/my/path/to/template';
        $data = ['doo' => 'bar'];
        $expected = md5($path . json_encode($data));
        $actual = $this->view->getTemplateId($path, $data);
        $this->assertEquals($expected, $actual);
    }

    public function testRender()
    {
        $this->engine->expects($this->once())
            ->method('render');

        $this->view->render('/', []);
    }

    public function testAddPath()
    {
        $this->engine->expects($this->once())
            ->method('addPath');

        $this->view->addPath('path', 'namespace');
    }
}
