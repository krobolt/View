<?php

namespace Ds\View;

use Ds\View\CacheInterface;

/**
 * Class Viewer
 * @package Ds\View
 */
class Viewer implements ViewInterface
{
    /**
     * @var EngineInterface
     */
    public $engine;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * Viewer constructor.
     *
     * @param EngineInterface $templateEngine
     * @param CacheInterface $cache
     */
    public function __construct(EngineInterface $templateEngine, CacheInterface $cache)
    {
        $this->engine = $templateEngine;
        $this->cache = $cache;
    }

    /**
     * With CacheInterface.
     * @param CacheInterface $cache
     * @return Viewer
     */
    public function withCache(CacheInterface $cache)
    {
        $new = clone $this;
        $new->cache = $cache;
        return $new;
    }

    /**
     * Replace Viewer Template Engine.
     *
     * @param EngineInterface $templateEngine
     * @return Viewer
     */
    public function withTemplateEngine(EngineInterface $templateEngine)
    {
        $new = clone $this;
        $new->engine = $templateEngine;
        return $new;
    }

    /**
     * Get Template Engine.
     *
     * @return EngineInterface
     */
    public function getTemplateEngine()
    {
        return $this->engine;
    }

    /**
     * Return Cache.
     *
     * @return CacheInterface|Cache
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Render template.
     *
     * @param string $path Path to template.
     * @param array $data Data to be passed to template.
     * @param array $options TemplateEngine render options.
     * @return string
     */
    public function render($path, array $data = array(), array $options = [])
    {
        $options = $this->formatOptions($options);
        $cacheKey = $this->getTemplateId($path, $data);

        if (true === $options['clearcache']) {
            $this->cache->delete($cacheKey);
        } elseif ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $content = $this->engine->render($path, $data, $options);

        if (isset($options['expires'])) {
            $this->cache->set($cacheKey, $content, $options['expires']);
        }

        return (string)$content;
    }

    /**
     * Catch any variations in options.
     *
     * @param array $options
     * @return array
     */
    private function formatOptions(array $options)
    {
        $opts = \array_change_key_case($options, CASE_LOWER);

        $opts['clearcache'] = isset($opts['clearcache']) ? $opts['clearcache'] : false;

        if (isset($opts['expire'])) {
            $opts['expires'] = $opts['expire'];
            unset($opts['expire']);
        }

        return $opts;
    }

    public function getTemplateId($path, $data)
    {
        return md5($path . json_encode($data));
    }

    /**
     * @param string $directory
     * @param string $namespace
     */
    public function addPath($directory, $namespace = '__main')
    {
        $this->engine->addPath($directory, $namespace);
    }
}
