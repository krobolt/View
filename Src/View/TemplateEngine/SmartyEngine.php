<?php

namespace Ds\View\TemplateEngine;

use Ds\View\EngineInterface;
use Smarty;

/**
 * Class SmartyEngine
 * @package Cyberhut\View\TemplateEngine
 */
class SmartyEngine implements EngineInterface
{

    /**
     * @var \Smarty
     */
    public $smarty;

    /**
     * Smarty constructor.
     * @param string $templatePath
     * @param string $compile
     * @param string $cache
     * @param string $config
     */
    public function __construct($templatePath = '', $compile = '', $cache = '', $config = '')
    {
        $this->smarty = new \Smarty();
        $this->smarty->setTemplateDir($templatePath);
        $this->smarty->setCompileDir($compile);
        $this->smarty->setCacheDir($cache);
        $this->smarty->setConfigDir($config);
    }

    /**
     * @param string $path
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public function render($path, array $data = [], array $options = [])
    {
        $cache = $options['cache'] ?: false;

        if ($cache === false || !$this->smarty->isCached('index.tpl')) {
            $this->smarty->assign('data', $data);
        }

        return $this->smarty->fetch(
            $path,
            md5(filter_input(INPUT_SERVER, 'REQUEST_URI'))
        );
    }

    /**
     * @param string $directory
     * @param string $namespace
     */
    public function addPath($directory = '', $namespace = '__main')
    {
        $this->smarty->addTemplateDir($directory, $namespace);
    }
}
