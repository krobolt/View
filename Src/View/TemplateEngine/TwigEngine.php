<?php

namespace Ds\View\TemplateEngine;

use Ds\View\EngineInterface;

/**
 * Class TwigEngine
 *
 * Updated Template Engine for Twig 2.0.
 *
 * @package Ds\View\TemplateEngine
 */
class TwigEngine implements EngineInterface
{

    /**
     * @var \Twig_Loader_Filesystem
     */
    public $loader;

    /**
     * @var \Twig_Environment
     */
    public $environment;

    /**
     * Twig constructor.
     *
     * @param \Twig_Loader_Filesystem $loader Twig Loader
     * @param \Twig_Environment $environment Twig Environment
     */
    public function __construct(
        \Twig_Loader_Filesystem $loader,
        \Twig_Environment $environment
    )
    {
        $this->loader = $loader;
        $this->environment = $environment;
    }

    /**
     * Render template.
     *
     * @param string $path Path to template.
     * @param array $data Data to be passed to template.
     * @param array $options TemplateEngine render options
     *
     * @return string
     */
    public function render($path, array $data = [], array $options = [])
    {
        $cacheView = $options['cache'] ?? false;
        $renderBlock = $options['block'] ?? false;
        $twigCacheDirectory = $this->environment->getCache();

        $cacheFilesystem = $this->getTwigCacheFilesystem();

        $templateCacheName = $cacheFilesystem->generateKey(
            $path, $this->environment->getTemplateClass($path)
        );

        if (!$cacheView || !$twigCacheDirectory) {
            $this->environment->setCache(false);
            $this->clearCacheFile($templateCacheName);
        }

        $template = $this->environment->load($path);

        if ($renderBlock) {
            return (string)$template->renderBlock($options['block'], $data);
        }

        return (string)$template->render($data);
    }

    /**
     * Get Twig Cache Filesystem.
     *
     * @return \Twig_Cache_Filesystem
     */
    public function getTwigCacheFilesystem()
    {
        $reflector = new \ReflectionClass('Twig_Environment');
        $prop = $reflector->getProperty('cache');
        $prop->setAccessible(true);
        return $prop->getValue($this->environment);
    }

    /**
     * Clear Template Cache file.
     *
     * @param string $filename
     */
    public function clearCacheFile(string $filename)
    {
        if (\file_exists($filename)) {
            \unlink($filename);
        }
    }

    /**
     * Get Cache template created Timestamp.
     *
     * @param \Twig_Cache_Filesystem $cacheFilesystem Twig Cache Filesystem.
     * @param string $path Cache template path.
     *
     * @return int
     */
    public function getCacheCreatedTime(\Twig_Cache_Filesystem $cacheFilesystem, $path)
    {
        return $cacheFilesystem->getTimestamp($path);
    }

    /**
     * Add path to Twig Loader.
     *
     * @param string $directory Template directory.
     * @param string $namespace Template Namespace.
     */
    public function addPath($directory, $namespace = '__main')
    {
        $this->loader->addPath($directory, $namespace);
    }

    /**
     * Get Twig Loader
     *
     * @return \Twig_LoaderInterface
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * Get Twig Environment
     *
     * @return \Twig_Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * With Twig Loader.
     *
     * @param \Twig_LoaderInterface $loader
     * @return TwigEngine
     */
    public function withLoader(\Twig_LoaderInterface $loader)
    {
        $new = clone $this;
        $new->loader = $loader;
        return $new;
    }

    /**
     * With Twig Environment.
     *
     * @param \Twig_Environment $environment
     * @return TwigEngine
     */
    public function withEnvironment(\Twig_Environment $environment)
    {
        $new = clone $this;
        $new->environment = $environment;
        return $new;
    }
}
