<?php

namespace Ds\View\TemplateEngine;

use Ds\View\EngineInterface;

/**
 * Class Twig1xEngine
 *
 * Template Engine for Twig 1.x
 *
 * @package Ds\View\TemplateEngine
 */
class Twig1xEngine implements EngineInterface
{

    /**
     * @var \Twig_LoaderInterface
     */
    public $loader;

    /**
     * @var \Twig_Environment
     */
    public $environment;

    /**
     * Twig constructor.
     *
     * @param \Twig_LoaderInterface $loader
     * @param \Twig_Environment $environment
     */
    public function __construct(\Twig_Loader_Filesystem $loader, \Twig_Environment $environment)
    {
        $this->loader = $loader;
        $this->environment = $environment;
    }

    /**
     * Render template.
     *
     * @param string $path Path to template.
     * @param array $data Data to be passed to template.
     * @param array $options TemplateEngine render options. ['block' => render a specific block from $path, 'cache' => compile twig]
     * @return string
     */
    public function render($path, array $data = array(), array $options = [])
    {
        //covert options to lower case.
        $options = $this->formatOptions($options);

        //Render only a section/block of the template.
        if (false !== $options['block']) {
            $template = $this->environment->loadTemplate($path);
            return (string)$template->renderBlock($options['block'], $data);
        }

        //Render the complete template.
        if ($options['cache'] === false) {
            $this->clearCacheFiles($path);
            $this->environment->setCache(false);
            $content = (string)$this->environment->render($path, $data);
        } else {
            //Render from compiled twig cache.
            $content = (string)$this->environment->render($path, $data);
        }

        return $content;
    }

    /**
     * Convert to lower case and check for options.
     * @param array $options
     * @return array
     */
    protected function formatOptions(array $options)
    {
        $opts = \array_change_key_case($options, CASE_LOWER);
        $opts['cache'] = isset($opts['cache']) ? $opts['cache'] : false;
        $opts['block'] = isset($opts['block']) ? $opts['block'] : false;
        return $opts;
    }

    public function clearCacheFiles($path, $clearAll = false)
    {
        $cacheFileName = $this->environment->getCacheFilename($path);
        $cacheFile = $this->environment->getCache();
        if ($cacheFile !== false) {
            if ($clearAll === true) {
                array_map('unlink', glob($cacheFile . "/*/*"));
            }
            if (\file_exists($cacheFileName)) {
                unlink($cacheFileName);
            }
        }
    }

    /**
     * @param string $directory
     * @param string $namespace
     */
    public function addPath($directory, $namespace = '__main')
    {
        $this->loader->addPath($directory, $namespace);
    }

    /**
     * @return \Twig_LoaderInterface
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * @return \Twig_Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @param \Twig_LoaderInterface $loader
     * @return Twig1xEngine
     */
    public function withLoader(\Twig_LoaderInterface $loader)
    {
        $new = clone $this;
        $new->loader = $loader;
        return $new;
    }

    /**
     * @param \Twig_Environment $environment
     * @return Twig1xEngine
     */
    public function withEnvironment(\Twig_Environment $environment)
    {
        $new = clone $this;
        $new->environment = $environment;
        return $new;
    }
}
