<?php

namespace Ds\View;

use Ds\View\CacheInterface;

/**
 * Interface ViewInterface
 * @package Ds\View
 */
interface ViewInterface extends EngineInterface
{
    /**
     * ViewInterface constructor.
     * @param EngineInterface $templateEngine
     * @param CacheInterface $cache
     */
    public function __construct(EngineInterface $templateEngine, CacheInterface $cache);

    /**
     * Return a new instance with CacheInterface.
     * @param CacheInterface $cache
     * @return ViewInterface
     */
    public function withCache(CacheInterface $cache);
}
