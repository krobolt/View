<?php
/** boot.php
 * Dan Smith <dsarchyuk@gmail.com>
 * Date: 10/04/2016
 * Time: 14:46
 */

define("ROOT", __DIR__);
define("SRC", __DIR__ . '/Src');

require_once './vendor/autoload.php';
